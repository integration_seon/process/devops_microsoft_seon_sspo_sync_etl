from pprint import pprint
import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
import logging
from devops_microsoft_client_etl.factory.factory import Factory
from devops_microsoft_client_etl.factory.service_enum import Service

logging.basicConfig(level=logging.INFO)

args = {
    'owner': 'seon',
    'start_date': airflow.utils.dates.days_ago(1)
}

dag = DAG (
    
    dag_id= 'ms_devops_sspo_data_base_integration',
    default_args=args,
    schedule_interval = None,
)


def __retrive_config(kwargs):
    
    conf = kwargs['dag_run'].conf
    organization_uuid = conf['organization_uuid']
    configuration_uuid = conf['configuration_uuid']
    secret = conf['secret']
    url = conf['url']

    data = {'organization_uuid': organization_uuid, 
                        "secret": secret, 
                        "url": url,
                        "configuration_uuid": configuration_uuid} 

    return organization_uuid, data

def retrive_scrum_projects(**kwargs):
    try:

        pprint ("retrive scrum project")
        
        organization_uuid, data = __retrive_config(kwargs)
        
        service = Factory.create(Service.ScrumProject)
        service.integrate(organization_uuid,data)

    except Exception as e: 
        logging.error("OS error: {0}".format(e))
        logging.error(e.__dict__)
    
def retrive_scrum_teams(**kwargs):
    pprint ("retrive scrum team")
    
    organization_uuid, data = __retrive_config(kwargs)
    
    service = Factory.create(Service.ProjectTeam)
    service.integrate(organization_uuid,data)

def retrive_team_members(**kwargs):
    pprint ("retrive scrum team")
    
    organization_uuid, data = __retrive_config(kwargs)
    
    service = Factory.create(Service.TeamMember)
    service.integrate(organization_uuid,data)

def retrive_sprints(**kwargs):
    pprint ("retrive sprints")
    
    organization_uuid, data = __retrive_config(kwargs)
    
    service = Factory.create(Service.Sprint)
    service.integrate(organization_uuid,data)


def retrive_user_stories(**kwargs):
    pprint ("retrive user stories")
    
    organization_uuid, data = __retrive_config(kwargs)
    service = Factory.create(Service.UserStory)
    service.integrate(organization_uuid,data)


def retrive_tasks(**kwargs):
    pprint ("retrive tasks")
    
    organization_uuid, data = __retrive_config(kwargs)
    
    service = Factory.create(Service.ScrumDevelopmentTask)
    service.integrate(organization_uuid,data)

    

def retrive_relationships(**kwargs):
    
    pprint ("retrive relationships")
    organization_uuid, data = __retrive_config(kwargs)
    
    service = Factory.create(Service.Product_backlog)
    service.integrate(organization_uuid,data)

## Operators
 
retrive_scrum_projects_operator = PythonOperator(
    task_id = 'retrive_scrum_projects',
    provide_context=True,
    python_callable=retrive_scrum_projects, 
    dag=dag,
)

retrive_scrum_teams_operator = PythonOperator(
    task_id = 'retrive_scrum_teams',
    provide_context=True,
    python_callable=retrive_scrum_teams, 
    dag=dag,
)

retrive_team_members_operator = PythonOperator(
    task_id = 'retrive_team_members',
    provide_context=True,
    python_callable=retrive_team_members, 
    dag=dag,
)

retrive_sprints_operator = PythonOperator(
    task_id = 'retrive_sprints',
    provide_context=True,
    python_callable=retrive_sprints, 
    dag=dag,
)

retrive_user_stories_operator = PythonOperator(
    task_id = 'retrive_user_stories',
    provide_context=True,
    python_callable=retrive_user_stories, 
    dag=dag,
)

retrive_tasks_operator = PythonOperator(
    task_id = 'retrive_tasks',
    provide_context=True,
    python_callable=retrive_tasks, 
    dag=dag,
)

retrive_relationships_operator = PythonOperator(
    task_id = 'retrive_relationships',
    provide_context=True,
    python_callable=retrive_relationships, 
    dag=dag,
)

## Workflow
retrive_scrum_projects_operator >> retrive_scrum_teams_operator >> retrive_team_members_operator >> retrive_sprints_operator >> retrive_user_stories_operator >> retrive_tasks_operator >> retrive_relationships_operator